# GroupingLPMs

### Setting it up
To be able to go through the notebooks and generate clustering results and the corresponding analyses, one should install all the packages listed in the environment.yml file. Additionally, to install the not yet published package consisting of process model similarity measures, use:

`pip install --no-deps --no-cache-dir --upgrade --force-reinstall git+https://github.com/VikiPeeva/promosim`